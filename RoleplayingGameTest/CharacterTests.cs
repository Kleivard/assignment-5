using System;
using Xunit;
using RoleplayingGame;
using RoleplayingGame.CustomException;

namespace RoleplayingGameTest
{

    public class CharacterTests
    {

        [Fact]
        public void Hero_Start_Level_One()
        {
            // Arrange
            Mage hero = new Mage() { Name = "Berit" };
            var expected = 1;

            // Act
            var result = hero.Level.CurrentLevel;

            //Assert
            Assert.Equal(expected, result);
        }

        // Not the best test when working with experiance.
        [Fact]
        public void Hero_Level_Increase_by_one()
        {
            // Arrange
            Mage hero = new Mage() { Name = "Name" };
            var experiance_worth_one_level = 100;
            var expected = 2;

            // Act
            hero.GainedExperiance(experiance_worth_one_level);
            var result = hero.Level.CurrentLevel;

            //Assert
            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData(-3f)]
        [InlineData(-0f)]
        public void Give_Zero_Or_Less_Experiance_To_Hero(float xp)
        {
            // Arrange
            Mage hero = new Mage() { Name = "Name" };

            // Act and Assert
            Assert.Throws<ArgumentException>(() => hero.GainedExperiance(xp));
        }

        [Fact]
        public void Mage_Primary_Stats_Check_At_Creation()
        {
            // Arrange
            Mage hero = new Mage() { Name = "Name" };
            PrimaryAttributes expected = new PrimaryAttributes() { Vitality = 5, Strength = 1, Dexterity = 1, Intelligence = 8 };
            // Act
            PrimaryAttributes result = hero.Attributes.BasePrimaryAttributes.GetAttribute();
            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Warrior_Primary_Stats_Check_At_Creation()
        {
            // Arrange
            Warrior hero = new Warrior() { Name = "Name" };
            PrimaryAttributes expected = new PrimaryAttributes() { Vitality = 10, Strength = 5, Dexterity = 2, Intelligence = 1 };
            // Act
            PrimaryAttributes result = hero.Attributes.TotalPrimaryAttributes;
            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Ranger_Primary_Stats_Check_At_Creation()
        {
            // Arrange
            Ranger hero = new Ranger() { Name = "Name" };
            PrimaryAttributes expected = new PrimaryAttributes() { Vitality = 8, Strength = 1, Dexterity = 7, Intelligence = 1 };
            // Act
            PrimaryAttributes result = hero.Attributes.BasePrimaryAttributes;
            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Rouge_Primary_Stats_Check_At_Creation()
        {
            // Arrange
            Rouge hero = new Rouge() { Name = "Name" };
            PrimaryAttributes expected = new PrimaryAttributes() { Vitality = 8, Strength = 2, Dexterity = 6, Intelligence = 1 };
            // Act
            PrimaryAttributes result = hero.Attributes.BasePrimaryAttributes;
            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Mage_Primary_Stats_Check_After_Leveling_Up()
        {
            // Arrange
            Mage hero = new Mage() { Name = "Name" };
            hero.GainedExperiance(100);
            PrimaryAttributes expected = new PrimaryAttributes() { Vitality = 8, Strength = 2, Dexterity = 2, Intelligence = 13 };
            // Act
            PrimaryAttributes result = hero.Attributes.BasePrimaryAttributes.GetAttribute();
            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Warrior_Primary_Stats_Check_After_Leveling_Up()
        {
            // Arrange
            Warrior hero = new Warrior() { Name = "Name" };
            hero.GainedExperiance(100);
            PrimaryAttributes expected = new PrimaryAttributes() { Vitality = 15, Strength = 8, Dexterity = 4, Intelligence = 2 };
            // Act
            PrimaryAttributes result = hero.Attributes.TotalPrimaryAttributes;
            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Range_Primary_Stats_Check_After_Leveling_Up()
        {
            // Arrange
            Ranger hero = new Ranger() { Name = "Name" };
            hero.GainedExperiance(100);
            PrimaryAttributes expected = new PrimaryAttributes() { Vitality = 10, Strength = 2, Dexterity = 12, Intelligence = 2 };
            // Act
            PrimaryAttributes result = hero.Attributes.BasePrimaryAttributes;
            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Rouge_Primary_Stats_Check_After_Leveling_Up()
        {
            // Arrange
            Rouge hero = new Rouge() { Name = "Name" };
            hero.GainedExperiance(100);
            PrimaryAttributes expected = new PrimaryAttributes() { Vitality = 11, Strength = 3, Dexterity = 10, Intelligence = 2 };
            // Act
            PrimaryAttributes result = hero.Attributes.BasePrimaryAttributes;
            //Assert
            Assert.Equal(expected, result);
        }
        [Fact]
        public void Hero_Secondary_Stats_Check_After_Leveling()
        {
            // Arrange
            Warrior hero = new Warrior() { Name = "Name" };
            hero.GainedExperiance(100);
            SecondaryAttributes expected = new SecondaryAttributes() { ArmorRating = 12, ElementalResistance = 2, Health = 150};
            // Act
            SecondaryAttributes result = hero.Attributes.SecondaryAttributes;
            //Assert
            Assert.Equal(expected, result);
        }
        
    }
}
