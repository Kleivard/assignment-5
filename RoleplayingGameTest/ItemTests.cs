﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using RoleplayingGame;
using RoleplayingGame.CustomException;

namespace RoleplayingGameTests
{
    public class ItemTests
    {
        [Fact]
        public void Equip_Higher_Weapon_Level_Than_Hero_Level()
        {
            // Arrange
            Warrior hero = new Warrior() { Name = "Name" };
            Weapon testHammer = new Weapon()
            {
                ItemName = "Gimli's Hammer",
                ItemLevelRequirment = 2,
                ItemSlot = ItemSlot.SLOT_WEAPON,
                WeaponType = WeaponType.WEP_HAMMER,
                WeaponAttributes = new WeaponAttributes() { BaseDamage = 40, AttackSpeed = 2.8f }
            };
            // Act and Assert
            Assert.Throws<LevelException>(() => hero.GearManager.CheckLevelRequirement(testHammer, hero.Level.CurrentLevel));
        }
        [Fact]
        public void Equip_Higher_Armor_Level_Than_Hero_Level()
        {
            // Arrange
            Warrior hero = new Warrior() { Name = "Name" };
            Armor testPlateHead = new Armor()
            {
                ItemName = "Common plate head armor",
                ItemLevelRequirment = 2,
                ItemSlot = ItemSlot.SLOT_HEAD,
                ArmorType = ArmorType.ARMOR_PLATE,
                ArmorAttributes = new PrimaryAttributes() { Vitality = 4, Strength = 4, Dexterity = 2, Intelligence = 0 }
            };
            // Act and Assert
            Assert.Throws<LevelException>(() => hero.GearManager.CheckLevelRequirement(testPlateHead, hero.Level.CurrentLevel));
        }
        [Fact]
        public void Equip_Wrong_Weapon_Type_On_Hero()
        {
            // Arrange
            Mage hero = new Mage() { Name = "Name" };
            Weapon testHammer = new Weapon()
            {
                ItemName = "Gimli's Hammer",
                ItemLevelRequirment = 1,
                ItemSlot = ItemSlot.SLOT_WEAPON,
                WeaponType = WeaponType.WEP_HAMMER,
                WeaponAttributes = new WeaponAttributes() { BaseDamage = 40, AttackSpeed = 2.8f }
            };
            // Act and Assert
            Assert.Throws<InvalidWeaponException>(() => hero.GearManager.CheckWeponTypeCompatibility(testHammer.WeaponType));
        }
        [Fact]
        public void Equip_Wrong_Armor_Type_On_Hero()
        {
            // Arrange
            Mage hero = new Mage() { Name = "Name" };
            Armor testLeatherBody = new Armor()
            {
                ItemName = "Common leather body armor",
                ItemLevelRequirment = 1,
                ItemSlot = ItemSlot.SLOT_BODY,
                ArmorType = ArmorType.ARMOR_LEATHER,
                ArmorAttributes = new PrimaryAttributes() { Vitality = 3, Strength = 1, Dexterity = 4, Intelligence = 1 }
            };
            // Act and Assert
            Assert.Throws<InvalidArmorException>(() => hero.GearManager.CheckArmorCompatibility(testLeatherBody.ArmorType));
        }

        [Fact]
        public void Equip_Correct_Weapon_Type()
        {
            // Arrange
            Mage hero = new Mage() { Name = "Name" };
            Weapon testWand = new Weapon()
            {
                ItemName = "Boring wand from Orc bones",
                ItemLevelRequirment = 1,
                ItemSlot = ItemSlot.SLOT_WEAPON,
                WeaponType = WeaponType.WEP_WAND,
                WeaponAttributes = new WeaponAttributes() { BaseDamage = 2, AttackSpeed = 2.8f }
            };
            String expected = "New weapon equipped!";
            // Act
            String result = hero.GearManager.EquipItem(testWand);
            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Equip_Correct_Armor_Type()
        {
            // Arrange
            Mage hero = new Mage() { Name = "Name" };
            Armor testClothHead = new Armor()
            {
                ItemName = "Common cloth head armor",
                ItemLevelRequirment = 1,
                ItemSlot = ItemSlot.SLOT_HEAD,
                ArmorType = ArmorType.ARMOR_CLOTH,
                ArmorAttributes = new PrimaryAttributes() { Vitality = 3, Strength = 1, Dexterity = 0, Intelligence = 5 }
            };
            String expected = "New armor equipped!";
            // Act
            String result = hero.GearManager.EquipItem(testClothHead);
            //Assert
            Assert.Equal(expected, result);
        }
        [Fact]
        public void Check_Level_One_DPS_Warrior()
        {
            // Arrange
            Warrior hero = new Warrior() { Name = "Name" };
            float expected = 1f * (1 + (5f / 100f));
            // Act
            float result = hero.DPS;
            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Check_Level_One_DPS_Warrior_With_Weapon()
        {
            // Arrange
            Warrior hero = new Warrior() { Name = "Name" };
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevelRequirment = 1,
                ItemSlot = ItemSlot.SLOT_WEAPON,
                WeaponType = WeaponType.WEP_AXE,
                WeaponAttributes = new WeaponAttributes() { BaseDamage = 7, AttackSpeed = 1.1f }
            };

            float expected = (7 * 1.1f) * (1 + (5f / 100f));
            // Act
            hero.EquipItem(testAxe);
            float result = hero.DPS;
            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Check_Level_One_DPS_Warrior_With_Weapon_And_Armor()
        {
            // Arrange
            Warrior hero = new Warrior() { Name = "Name" };
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevelRequirment = 1,
                ItemSlot = ItemSlot.SLOT_WEAPON,
                WeaponType = WeaponType.WEP_AXE,
                WeaponAttributes = new WeaponAttributes() { BaseDamage = 7, AttackSpeed = 1.1f }
            };
            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevelRequirment = 1,
                ItemSlot = ItemSlot.SLOT_BODY,
                ArmorType = ArmorType.ARMOR_PLATE,
                ArmorAttributes = new PrimaryAttributes() { Vitality = 5, Strength = 1, Dexterity = 0, Intelligence = 0 }
            };
            float expected = (7 * 1.1f) * (1 + ((5 + 1) / 100f));
            // Act
            hero.EquipItem(testAxe);
            hero.EquipItem(testPlateBody);
            float result = hero.DPS;
            //Assert
            Assert.Equal(expected, result);
        }
    }
}
