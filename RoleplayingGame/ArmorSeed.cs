﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoleplayingGame
{    
     /// <summary>
     /// Singelton class to generate a list conatining sample Armor available for Hero class.
     /// </summary>
    public sealed class ArmorSeed
    {
        private List<Armor> armorInventory = new List<Armor>();
        private static readonly ArmorSeed instance = new ArmorSeed();

        static ArmorSeed() 
        {
        }
        private ArmorSeed() 
        {
            CreateInventory();
        }
        /// <summary>
        /// Getter for sample armor.
        /// <returns>
        /// Returns an List with the type Armor with preset armor.
        /// </returns>
        /// </summary>
        public static List<Armor> Instance
        {
            get
            {
                return instance.armorInventory;
            }
        }
        /// <summary>
        /// Generate predefined armor of all armor types.
        /// </summary>
        private void CreateInventory()
        {

            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevelRequirment = 1,
                ItemSlot = ItemSlot.SLOT_BODY,
                ArmorType = ArmorType.ARMOR_PLATE,
                ArmorAttributes = new PrimaryAttributes() { Vitality = 5, Strength = 1, Dexterity = 0, Intelligence = 0 }
            };

            Armor testClothHead = new Armor()
            {
                ItemName = "Common cloth head armor",
                ItemLevelRequirment = 1,
                ItemSlot = ItemSlot.SLOT_HEAD,
                ArmorType = ArmorType.ARMOR_CLOTH,
                ArmorAttributes = new PrimaryAttributes() { Vitality = 3, Strength = 1, Dexterity = 0, Intelligence = 5 }
            };

            Armor testLeatherBody = new Armor()
            {
                ItemName = "Common leather body armor",
                ItemLevelRequirment = 2,
                ItemSlot = ItemSlot.SLOT_BODY,
                ArmorType = ArmorType.ARMOR_LEATHER,
                ArmorAttributes = new PrimaryAttributes() { Vitality = 3, Strength = 1, Dexterity = 4, Intelligence = 1 }
            };

            Armor testLeatherhLeg = new Armor()
            {
                ItemName = "Common leather leg armor",
                ItemLevelRequirment = 3,
                ItemSlot = ItemSlot.SLOT_LEG,
                ArmorType = ArmorType.ARMOR_LEATHER,
                ArmorAttributes = new PrimaryAttributes() { Vitality = 7, Strength = 1, Dexterity = 7, Intelligence = 0 }
            };

            Armor testPlateHead = new Armor()
            {
                ItemName = "Common plate head armor",
                ItemLevelRequirment = 2,
                ItemSlot = ItemSlot.SLOT_HEAD,
                ArmorType = ArmorType.ARMOR_PLATE,
                ArmorAttributes = new PrimaryAttributes() { Vitality = 4, Strength = 4, Dexterity = 2, Intelligence = 0 }
            };

            Armor testMailHead = new Armor()
            {
                ItemName = "Common mail head armor",
                ItemLevelRequirment = 5,
                ItemSlot = ItemSlot.SLOT_HEAD,
                ArmorType = ArmorType.ARMOR_MAIL,
                ArmorAttributes = new PrimaryAttributes() { Vitality = 17, Strength = 4, Dexterity = 7, Intelligence = 0 }
            };

            armorInventory.AddRange(new List<Armor> {testPlateBody, testClothHead, testLeatherBody, testLeatherhLeg, testPlateHead, testMailHead });
            
        }
    }
}
