﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoleplayingGame.CustomException
{
    class GearException : Exception
    {
        public GearException(string message) : base(message) { }
        public override string Message => "Gear exception: ";
    }
}
