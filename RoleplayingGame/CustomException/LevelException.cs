﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoleplayingGame.CustomException
{
    public class LevelException : Exception
    {
        public LevelException(string message) : base(message) { }

        public override string Message => "Level exception: ";
    }
}
