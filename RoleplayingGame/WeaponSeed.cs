﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoleplayingGame
{
    /// <summary>
    /// Singelton class to generate a list conatining sample weapons available for Hero class.
    /// </summary>
    class WeaponSeed
    {
        private List<Weapon> weaponInventory = new List<Weapon>();
        private static readonly WeaponSeed instance = new WeaponSeed();

        static WeaponSeed(){}
        private WeaponSeed()
        {
            CreateInventory();
        }
        /// <summary>
        /// Getter for sample weapons.
        /// <returns>
        /// Returns an List with the type Weapon with preset weapons.
        /// </returns>
        /// </summary>
        public static List<Weapon> Instance
        {
            get
            {
                return instance.weaponInventory;
            }
        }
        /// <summary>
        /// Generate predefined weapons of all Weapon types
        /// </summary>
        private void CreateInventory()
        {
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevelRequirment = 1,
                ItemSlot = ItemSlot.SLOT_WEAPON,
                WeaponType = WeaponType.WEP_AXE,
                WeaponAttributes = new WeaponAttributes() { BaseDamage = 7, AttackSpeed = 1.1f }
            };

            Weapon testBow = new Weapon()
            {
                ItemName = "Common bow",
                ItemLevelRequirment = 1,
                ItemSlot = ItemSlot.SLOT_WEAPON,
                WeaponType = WeaponType.WEP_BOW,
                WeaponAttributes = new WeaponAttributes() { BaseDamage = 12, AttackSpeed = 0.8f }
            };
            Weapon testDagger = new Weapon()
            {
                ItemName = "Axe of Luden's",
                ItemLevelRequirment = 1,
                ItemSlot = ItemSlot.SLOT_WEAPON,
                WeaponType = WeaponType.WEP_DAGGER,
                WeaponAttributes = new WeaponAttributes() { BaseDamage = 14, AttackSpeed = 0.8f }
            };

            Weapon testHammer = new Weapon()
            {
                ItemName = "Gimli's Hammer",
                ItemLevelRequirment = 2,
                ItemSlot = ItemSlot.SLOT_WEAPON,
                WeaponType = WeaponType.WEP_HAMMER,
                WeaponAttributes = new WeaponAttributes() { BaseDamage = 40, AttackSpeed = 2.8f }
            };
            Weapon testStaff = new Weapon()
            {
                ItemName = "Gandalfs Staff",
                ItemLevelRequirment = 1,
                ItemSlot = ItemSlot.SLOT_WEAPON,
                WeaponType = WeaponType.WEP_STAFF,
                WeaponAttributes = new WeaponAttributes() { BaseDamage = 14, AttackSpeed = 1.4f }
            };

            Weapon testSword = new Weapon()
            {
                ItemName = "Excalibur",
                ItemLevelRequirment = 1,
                ItemSlot = ItemSlot.SLOT_WEAPON,
                WeaponType = WeaponType.WEP_SWORD,
                WeaponAttributes = new WeaponAttributes() { BaseDamage = 5, AttackSpeed = 5.8f }
            };

            Weapon testWand = new Weapon()
            {
                ItemName = "Boring wand from Orc bones",
                ItemLevelRequirment = 1,
                ItemSlot = ItemSlot.SLOT_WEAPON,
                WeaponType = WeaponType.WEP_WAND,
                WeaponAttributes = new WeaponAttributes() { BaseDamage = 2, AttackSpeed = 2.8f }
            };

            weaponInventory.AddRange(new List<Weapon> { testAxe , testBow, testWand, testDagger, testHammer, testStaff, testSword});

        }
    }
}
