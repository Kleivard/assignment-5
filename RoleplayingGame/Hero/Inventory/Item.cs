﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoleplayingGame
{
    /// <summary>
    /// Abstract class for Weapon and Armor.
    /// <para>Contains: ItemName, ItemLevel, and ItemSlot.</para>
    /// </summary>
    public abstract class Item
    {
        public string ItemName { get; set; }
        public int ItemLevelRequirment { get; set; }
        /// <summary>
        /// What item slot the Item holds when used by hero.
        /// </summary>
        public ItemSlot ItemSlot { get; set; }

    }
}
