﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RoleplayingGame.CustomException;

namespace RoleplayingGame
{ 
    /// <summary>
    /// GearManager is used to manage Items (Item) used by Hero class as equipping/unequipping items.
    /// <para>GearManager implements interface I_ItemCompatibility and checks 
    /// Item type compatibility, item level compatibility, and availible item slots.</para>
    /// </summary>
    public class GearManager : I_ItemCompatibility
    {
        /// <summary>
        /// Gear hold the current items that are equipped on the hero
        /// </summary>
        public Dictionary<ItemSlot, Item> Gear { set; get; } = new Dictionary<ItemSlot, Item>();
        /// <summary>
        /// Holds the weapons that can be used by class that implements GearManager.
        /// </summary>
        public List<WeaponType> WeaponCompatibility { get; set; } = new List<WeaponType>();
        /// <summary>
        /// Holds the armors that can be used by class that implements GearManager.
        /// </summary>
        public List<ArmorType> ArmorCompatibility { get; set; } = new List<ArmorType>();

        /// <summary>
        /// Used when unequipping items. Used to display an empty weapon slot.
        /// </summary>
        private Weapon WEAPON_SLOT_EMPTY = new Weapon()
        {
            ItemName = "Empty",
            ItemLevelRequirment = 0,
            ItemSlot = ItemSlot.SLOT_WEAPON,
            WeaponType = WeaponType.WEP_NONE,
            WeaponAttributes = new WeaponAttributes() { BaseDamage = 0, AttackSpeed = 0f }
        };
        /// <summary>
        /// Used when unequipping items. Used to display an empty armor slot.
        /// </summary>
        private Armor ARMOR_SLOT_EMPTY = new Armor()
        {
            ItemName = "Empty",
            ItemLevelRequirment = 0,
            ItemSlot = ItemSlot.SLOT_BODY,
            ArmorType = ArmorType.ARMOR_NONE,
            ArmorAttributes = new PrimaryAttributes()
        };
        /// <summary>
        /// Adds 4 slots to the gear for equipping: Weapon, Head, Body and Leg
        /// </summary>
        public GearManager()
        {
            Gear.Add(ItemSlot.SLOT_WEAPON, WEAPON_SLOT_EMPTY);
            Gear.Add(ItemSlot.SLOT_HEAD, ARMOR_SLOT_EMPTY);
            Gear.Add(ItemSlot.SLOT_BODY, ARMOR_SLOT_EMPTY);
            Gear.Add(ItemSlot.SLOT_LEG, ARMOR_SLOT_EMPTY);
        }
        /// <summary>
        /// Checks if Weapon type is compaible with Hero.
        /// </summary>
        /// <exception cref="InvalidWeaponException">Thrown when weapon is not compatible with Hero</exception>
        /// <param name="weapon"></param>
        public void CheckWeponTypeCompatibility(WeaponType weapon)
        {
            foreach (WeaponType compatiblWeapon in WeaponCompatibility)
            {
                if (compatiblWeapon.Equals(weapon))
                {
                    return;
                }
            }
            throw new InvalidWeaponException("");
        }
        /// <summary>
        /// Checks if Armor type is compaible with Hero.
        /// </summary>
        /// <exception cref="InvalidArmorException">Thrown when Armor is not compatible with Hero</exception>
        /// <param name="weapon"></param>
        public void CheckArmorCompatibility(ArmorType armor)
        {
            foreach (ArmorType compatibleArmor in ArmorCompatibility)
            {
                    if (compatibleArmor.Equals(armor))
                    {
                        return;
                    }
            }
            throw new InvalidArmorException("");
        }
        /// <summary>
        /// Checks so hero is higher or equal to the Items level. Requirment when equpping items.
        /// </summary>
        /// <exception cref="LevelException">Hero lower level then Item</exception>
        /// <param name="item"></param>
        /// <param name="currentHeroLevel"></param>
        public void CheckLevelRequirement(Item item, int currentHeroLevel)
        {
            if (item.ItemLevelRequirment > currentHeroLevel)
            {
                throw new LevelException(" ");
            }
        }
        /// <summary>
        /// Checks if slot is available.
        /// </summary>
        /// <param name="itemslot">Slot to check.</param>
        /// <exception cref="GearException">Thrown when no slot available</exception>
        /// <returns>Returns true if item slot is available to use, else false</returns>
        public bool IsItemSlotAvailable(ItemSlot itemslot) {

            switch (itemslot)
            {
                case ItemSlot.SLOT_HEAD:
                    Armor tempHead = (Armor)Gear[ItemSlot.SLOT_HEAD];
                    return tempHead.ArmorType == ArmorType.ARMOR_NONE;
                case ItemSlot.SLOT_BODY:
                    Armor tempBody = (Armor)Gear[ItemSlot.SLOT_BODY];
                    return tempBody.ArmorType == ArmorType.ARMOR_NONE;
                case ItemSlot.SLOT_LEG:
                    Armor tempLeg = (Armor)Gear[ItemSlot.SLOT_LEG];
                    return tempLeg.ArmorType == ArmorType.ARMOR_NONE;
                case ItemSlot.SLOT_WEAPON:
                    Weapon tempWep = (Weapon)Gear[ItemSlot.SLOT_WEAPON];
                    return tempWep.WeaponType == WeaponType.WEP_NONE;
                default:
                    throw new GearException("");
            }
        } 
        /// <summary>
        /// Adds item to Gear list in GearManager.
        /// </summary>
        /// <param name="item"></param>
        public string EquipItem(Weapon item)
        {
            if (item.WeaponType == WeaponType.WEP_NONE)
            {
                return " ";
            }
            Gear[item.ItemSlot] = item;
            return "New weapon equipped!";         
        }
        public string EquipItem(Armor item)
        {
            if (item is Armor)
            {

            }
            if (item.ArmorType == ArmorType.ARMOR_NONE)
            {
                return " ";
            }
            Gear[item.ItemSlot] = (Armor)item;
            return "New armor equipped!";
        }
        /// <summary>
        /// Removes Item from Gear list in GearManager based on the slot that should be removed.
        /// </summary>
        /// <param name="item">Item slot that should be removed</param>
        public void UnEquipWeapon(ItemSlot itemslot)
        {
            Weapon wep = (Weapon) Gear[itemslot];
            Gear[itemslot] = WEAPON_SLOT_EMPTY;
            Console.WriteLine($"You unequipped {itemslot}, removed '{wep.ItemName}'");
        }
        public void UnEquipArmor(ItemSlot itemslot)
        {
            Armor arm = (Armor)Gear[itemslot];
            Gear[itemslot] = ARMOR_SLOT_EMPTY;
            Console.WriteLine($"You unequipped {itemslot}, removed '{arm.ItemName}'");
        }
    }
}
