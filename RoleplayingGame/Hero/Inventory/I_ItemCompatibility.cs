﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoleplayingGame
{
    // TODO: split up in 3 interfaces, 1 parent and 2 child for armor and weapon.
    /// <summary>
    /// Used to contract compatibility checks for Itemsclass
    /// </summary>
    public interface I_ItemCompatibility
    {
        /// <summary>
        /// Implement weapon compatibility check with Hero subclass.
        /// </summary>
        /// <param name="weapon"></param>
        public void CheckWeponTypeCompatibility(WeaponType weapon);
        /// <summary>
        /// Implement weapon compatibility check with Hero subclass.
        /// </summary>
        /// <param name="weapon"></param>
        public void CheckArmorCompatibility(ArmorType armor);
        /// <summary>
        /// Implement item level check for armors/weapons with Hero.
        /// </summary>
        /// <param name="weapon"></param>
        public void CheckLevelRequirement(Item item, int currentHeroLevel);
    }
}
