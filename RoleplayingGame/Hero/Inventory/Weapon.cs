﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoleplayingGame
{
    /// <summary>
    /// Child class to Item. Weapon contains specific weapon attributes
    /// </summary>
    public class Weapon : Item
    {
        /// <summary>
        /// WeaponAttributes contain base damage and attack speed
        /// </summary>
        public WeaponAttributes WeaponAttributes;
        /// <summary>
        /// <ref>Types.WeaponType contain all weapon types</ref>
        /// </summary>
        public WeaponType WeaponType { get; set; }
        /// <summary>
        /// Calculates weapon DPS
        /// </summary>
        /// <returns>Base damage times attack speed</returns>
        public float GetWeaponDPS()
        {
            return WeaponAttributes.AttackSpeed * WeaponAttributes.BaseDamage;
        }

        public override string ToString()
        {
            return $"Weapon Info: Name = {ItemName},\t Ilvl = {ItemLevelRequirment},\t WepType = {WeaponType},\t DMG = {WeaponAttributes.BaseDamage},\t AttackSpeed = {WeaponAttributes.AttackSpeed}";
        }
    }
}
