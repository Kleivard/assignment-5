﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoleplayingGame
{
    /// <summary>
    /// Child class to Item. Armor contains specific armor attributes
    /// </summary>
    public class Armor : Item
    {
        /// <summary>
        /// Armor uses PrimaryAttributes as bonus stats.
        /// </summary>
        public PrimaryAttributes ArmorAttributes { get; set; }
        /// <summary>
        /// <ref>Types.ArmorType contain all armor types</ref>
        /// </summary>
        public ArmorType ArmorType { get; set; }
        public override string ToString()
        {
            return $"Armor Info: Name = {ItemName},\t Ilvl = {ItemLevelRequirment},\t ArmorType = {ArmorType}," +
                $" Int = {ArmorAttributes.Intelligence}, Dex = {ArmorAttributes.Dexterity}, Str = {ArmorAttributes.Strength}, Vit = {ArmorAttributes.Vitality}";
        }
    }
}
