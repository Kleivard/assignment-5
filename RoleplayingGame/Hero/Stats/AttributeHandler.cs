﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoleplayingGame
{
    /// <summary>
    /// AttributeHandler is used for calculations primary and secondary attribues for a hero.
    /// <para>Handels increasing, reevaluation of all attributes (gear, primary and secondary)</para>
    /// </summary>
    public class AttributeHandler
    {
        /// <summary>
        /// Primary attributes without gear.
        /// </summary>
        public PrimaryAttributes BasePrimaryAttributes { get; set; }
        /// <summary>
        /// Primary attributes with gear included.
        /// </summary>
        public PrimaryAttributes TotalPrimaryAttributes { get; set; }
        /// <summary>
        /// Primary attributes for current equipped gear.
        /// </summary>
        public PrimaryAttributes EquippedPrimaryAttributes { get; set; } = new PrimaryAttributes() { Intelligence = 0, Strength = 0, Dexterity = 0, Vitality = 0 };
        /// <summary>
        /// Adds new item attributes to EquippedPrimaryAttributes
        /// </summary>
        /// <param name="armorAttributes">Item attributes to add</param>
        public void IncreaseEquippedPrimaryAttributes(PrimaryAttributes armorAttributes) => EquippedPrimaryAttributes += armorAttributes;
        /// <summary>
        /// Subtract new item attributes from EquippedPrimaryAttributes
        /// </summary>
        /// <param name="armorAttributes">Item attributes to add</param>
        public void DecreaseEquippedPrimaryAttributes(PrimaryAttributes armorAttributes) => EquippedPrimaryAttributes -= armorAttributes;
        /// <summary>
        /// Adds Equipped and Base attributes
        /// </summary>
        public void UpdateTotalPrimaryAttributes() => TotalPrimaryAttributes = EquippedPrimaryAttributes + BasePrimaryAttributes;
        /// <summary>
        /// Base attributes are increased from leveling up.
        /// </summary>
        public void IncreaseBasePrimaryAttributes() => BasePrimaryAttributes += LevelUpPrimaryAttributes;
        public PrimaryAttributes StartPrimaryAttributes { get; set; }
        public PrimaryAttributes LevelUpPrimaryAttributes { get; set; }
        public SecondaryAttributes SecondaryAttributes { get; set; } = new SecondaryAttributes() { Health = 0, ArmorRating = 0, ElementalResistance = 0 };
        /// <summary>
        /// Calaculates secondary attributes based on primary attributes.
        /// </summary>
        public void UpdateSecondaryAttributes() {
            SecondaryAttributes.Health = TotalPrimaryAttributes.Vitality * 10;
            SecondaryAttributes.ArmorRating = TotalPrimaryAttributes.Strength + TotalPrimaryAttributes.Dexterity;
            SecondaryAttributes.ElementalResistance = TotalPrimaryAttributes.Intelligence;
        }
        /// <summary>
        /// Initiates TotalPrimaryAttributes based on BasePrimaryAttributes and evaluates secondary attributes.
        /// </summary>
        public void InitAllAttributes() {
            BasePrimaryAttributes = StartPrimaryAttributes;
            TotalPrimaryAttributes = BasePrimaryAttributes;
            UpdateSecondaryAttributes();
        }
        /// <summary>
        /// Recalculates Total and Seocndary attributes.
        /// </summary>
        public void UpdateAllAttributes()
        {
            UpdateTotalPrimaryAttributes();
            UpdateSecondaryAttributes();
        }
    }
}
