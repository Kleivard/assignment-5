﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoleplayingGame
{
    /// <summary>
    /// Primary attributes contain: Strength, Dexterity, Intelligence and Vitality as integers.
    /// </summary>
    public class PrimaryAttributes
    {   
        /// <summary>
        /// Strength affect armor and Warrior damage
        /// </summary>
        public int Strength { get; set; } = 0;
        /// <summary>
        /// Dexterity affects armor and Ranger/Rouge damage
        /// </summary>
        public int Dexterity { get; set; } = 0;
        /// <summary>
        /// Intelligence affect elemental resistance and mage damage.
        /// </summary>
        public int Intelligence { get; set; } = 0;
        /// <summary>
        /// Increases the life of a hero.
        /// </summary>
        public int Vitality { get; set; } = 0;
        /// <summary>
        /// Write all attributes to console.
        /// </summary>
        /// <returns>All the attributes</returns>
        public override string ToString()
        {
            return $"Strength:\t{Strength}\nDexterity:\t{Dexterity}\nIntelligence:\t{Intelligence}\nVitality:\t{Vitality}\n";
        }
        /// <summary>
        /// Overloaded '+' operator to add same object.
        /// <para>Adds int with int, str with str, dex with dex, and vit with vit</para>
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        public static PrimaryAttributes operator +(PrimaryAttributes lhs, PrimaryAttributes rhs) {
            return new PrimaryAttributes
            {
                Strength = lhs.Strength + rhs.Strength,
                Dexterity = lhs.Dexterity + rhs.Dexterity,
                Intelligence = lhs.Intelligence + rhs.Intelligence,
                Vitality = lhs.Vitality + rhs.Vitality
            };
        }

        public override bool Equals(object obj)
        {
            PrimaryAttributes ls = (PrimaryAttributes)obj;
            if (this.Dexterity == ls.Dexterity && this.Strength == ls.Strength && this.Vitality == ls.Vitality && this.Intelligence == ls.Intelligence)
            {
                return true;
            }
            else if (base.Equals(obj)) {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Overloaded '+' operator to add same object.
        /// <para>Subtracts int with int, str with str, dex with dex, and vit with vit</para>
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        public static PrimaryAttributes operator -(PrimaryAttributes lhs, PrimaryAttributes rhs)
        {
            return new PrimaryAttributes
            {
                Strength = lhs.Strength - rhs.Strength,
                Dexterity = lhs.Dexterity - rhs.Dexterity,
                Intelligence = lhs.Intelligence - rhs.Intelligence,
                Vitality = lhs.Vitality - rhs.Vitality
            };
        }
        public PrimaryAttributes GetAttribute() { return this; }
    }
    /// <summary>
    /// Secondary attributes builds on primary attribues.
    /// <para>Contains: Health, Armor Rating, and Elemental Resistance</para>
    /// </summary>
    public class SecondaryAttributes
    {
        /// <summary>
        /// Health = vitality * 10
        /// </summary>
        public int Health { get; set; }
        /// <summary>
        /// Armor Rating = Dexterity + Strength
        /// </summary>
        public int ArmorRating { get; set; }
        /// <summary>
        /// Elemental Resistance = Intelligence
        /// </summary>
        public int ElementalResistance { get; set; }

        public override string ToString()
        {
            return $"Health:\t\t\t{Health}\nArmor Rating:\t\t{ArmorRating}\nElemental Resistance:\t{ElementalResistance}\n";
        }
        public override bool Equals(object obj)
        {
            SecondaryAttributes ls = (SecondaryAttributes)obj;
            if (this.Health == ls.Health && this.ArmorRating == ls.ArmorRating && this.ElementalResistance == ls.ElementalResistance)
            {
                return true;
            }
            else if (base.Equals(obj))
            {
                return true;
            }

            return false;
        }
    }
        /// <summary>
        /// Attributes on weapon.
        /// <para>Conatins basedamage and attack speed</para>
        /// </summary>
    public class WeaponAttributes
    {
        public int BaseDamage { get; set; }
        public float AttackSpeed { get; set; }
    }
    
}
