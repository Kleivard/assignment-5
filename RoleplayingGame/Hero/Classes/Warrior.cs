﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoleplayingGame
{
    /// <summary>
    /// Subclass for Hero. Contain specific Warrior attributes.
    /// </summary>
    public class Warrior : Hero
    {
        public override ClassType ClassType { get; set; } = ClassType.WARRIOR;
        /// <summary>
        /// Constructor set predefined values.
        /// <para>Initialize base attributes and level up attributes for Warrior</para>
        /// <para>Initialize Armor and Weapon compatibility for Warrior </para>
        /// </summary>
        public Warrior()
        {
            Attributes.LevelUpPrimaryAttributes = new PrimaryAttributes()
            {
                Strength = 3,
                Dexterity = 2,
                Intelligence = 1,
                Vitality = 5
            };

            Attributes.StartPrimaryAttributes = new PrimaryAttributes()
            {
                Strength = 5,
                Dexterity = 2,
                Intelligence = 1,
                Vitality = 10
            };

            GearManager.ArmorCompatibility.Add(ArmorType.ARMOR_MAIL);
            GearManager.ArmorCompatibility.Add(ArmorType.ARMOR_PLATE);

            GearManager.WeaponCompatibility.Add(WeaponType.WEP_AXE);
            GearManager.WeaponCompatibility.Add(WeaponType.WEP_HAMMER);
            GearManager.WeaponCompatibility.Add(WeaponType.WEP_SWORD);

            base.Init();
        }
        /// <summary>
        /// Overrides DPS get method from Hero to apply hero specific attribute bonus to weapn DPS.
        /// </summary>
        /// <return>Returns the DPS for hero.</return>
        public override float DPS
        {
            get
            {
                Weapon wep = (Weapon)GearManager.Gear[ItemSlot.SLOT_WEAPON];
                if (!wep.WeaponType.Equals(WeaponType.WEP_NONE))
                {
                    return wep.GetWeaponDPS() * (1f + (Attributes.TotalPrimaryAttributes.Strength / 100f));
                }
                else
                {
                    return (1f * (1 + Attributes.TotalPrimaryAttributes.Strength / 100f));
                }
            }
        }        
    }
}
