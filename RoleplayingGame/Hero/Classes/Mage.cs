﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoleplayingGame
{
    /// <summary>
    /// Subclass for Hero. Contain specific Mage attributes.
    /// </summary>
    public class Mage : Hero
    {
        public override ClassType ClassType { get; set; } = ClassType.MAGE;
        /// <summary>
        /// Constructor set predefined values.
        /// <para>Initialize base attributes and level up attributes for Rouge.</para>
        /// <para>Initialize Armor and Weapon compatibility for Rouge.</para>
        /// </summary>
        public Mage()
        {
            Attributes.LevelUpPrimaryAttributes = new PrimaryAttributes()
            {
                Strength = 1,
                Dexterity = 1,
                Intelligence = 5,
                Vitality = 3
            };

            Attributes.StartPrimaryAttributes = new PrimaryAttributes()
            {
                Strength = 1,
                Dexterity = 1,
                Intelligence = 8,
                Vitality = 5
            };

            GearManager.ArmorCompatibility.Add(ArmorType.ARMOR_CLOTH);

            GearManager.WeaponCompatibility.Add(WeaponType.WEP_STAFF);
            GearManager.WeaponCompatibility.Add(WeaponType.WEP_WAND);

            base.Init();
        }
        /// <summary>
        /// Overrides DPS get method from Hero to apply hero specific attribute bonus to weapn DPS.
        /// </summary>
        /// <return>Returns the DPS for hero.</return>
        public override float DPS
        {
            get
            {
                Weapon wep = (Weapon)GearManager.Gear[ItemSlot.SLOT_WEAPON];

                if (wep.WeaponType.Equals(WeaponType.WEP_NONE)) 
                {
                    return 1f * (1f + Attributes.TotalPrimaryAttributes.Intelligence / 100f);
                    
                }else
                {
                    return (int)(wep.GetWeaponDPS() * (1f + Attributes.TotalPrimaryAttributes.Intelligence / 100f));
                }
            }
        }
    }
}