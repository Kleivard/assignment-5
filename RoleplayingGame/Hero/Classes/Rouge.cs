﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoleplayingGame
{
    /// <summary>
    /// Subclass for Hero. Contain specific Rouge attributes.
    /// </summary>
    public class Rouge : Hero
    {
        public override ClassType ClassType { get; set; } = ClassType.ROUGE;
        /// <summary>
        /// Constructor set predefined values.
        /// <para>Initialize base attributes and level up attributes for Rouge</para>
        /// <para>Initialize Armor and Weapon compatibility for Rouge </para>
        /// </summary>
        public Rouge() {

            Attributes.StartPrimaryAttributes = new PrimaryAttributes()
            {
                Strength = 2,
                Dexterity = 6,
                Intelligence = 1,
                Vitality = 8
            };

            Attributes.LevelUpPrimaryAttributes = new PrimaryAttributes()
            {
                Strength = 1,
                Dexterity = 4,
                Intelligence = 1,
                Vitality = 3
            };

            GearManager.ArmorCompatibility.Add(ArmorType.ARMOR_LEATHER);
            GearManager.ArmorCompatibility.Add(ArmorType.ARMOR_MAIL);

            GearManager.WeaponCompatibility.Add(WeaponType.WEP_DAGGER);
            GearManager.WeaponCompatibility.Add(WeaponType.WEP_SWORD);

            base.Init();
        }
        /// <summary>
        /// Overrides DPS get method from Hero to apply hero specific attribute bonus to weapn DPS.
        /// </summary>
        /// <return>Returns the DPS for hero.</return>
        public override float DPS
        {
            get
            {
                Weapon wep = (Weapon)GearManager.Gear[ItemSlot.SLOT_WEAPON];
                if (!wep.WeaponType.Equals(WeaponType.WEP_NONE))
                {

                    return (int)wep.GetWeaponDPS() * (1f + Attributes.TotalPrimaryAttributes.Dexterity / 100f);
                }
                else
                {
                    return (int)(1f * (1f + Attributes.TotalPrimaryAttributes.Dexterity / 100f));
                }
            }
        }
    }
}
