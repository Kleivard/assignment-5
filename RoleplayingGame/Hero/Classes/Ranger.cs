﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoleplayingGame
{
    /// <summary>
    /// Subclass for Hero. Contain specific Ranger attributes.
    /// </summary>
    public class Ranger : Hero
    {
        public override ClassType ClassType { get; set; } = ClassType.RANGER;
        /// <summary>
        /// Constructor set predefined values.
        /// <para>Initialize base attributes and level up attributes for Rouge</para>
        /// <para>Initialize Armor and Weapon compatibility for Rouge </para>
        /// </summary>
        public Ranger() {

            Attributes.StartPrimaryAttributes = new PrimaryAttributes()
            {
                Strength = 1,
                Dexterity = 7,
                Intelligence = 1,
                Vitality = 8
            };

            Attributes.LevelUpPrimaryAttributes = new PrimaryAttributes()
            {
                Strength = 1,
                Dexterity = 5,
                Intelligence = 1,
                Vitality = 2
            };

            GearManager.ArmorCompatibility.Add(ArmorType.ARMOR_LEATHER);
            GearManager.ArmorCompatibility.Add(ArmorType.ARMOR_MAIL);

            GearManager.WeaponCompatibility.Add(WeaponType.WEP_BOW);

            base.Init();
        }

        /// <summary>
        /// Overrides DPS get method from Hero to apply hero specific attribute bonus to weapn DPS.
        /// </summary>
        /// <return>Returns the DPS for hero.</return>
        public override float DPS
        {
            get {
                Weapon wep = (Weapon)GearManager.Gear[ItemSlot.SLOT_WEAPON];
                if (!wep.WeaponType.Equals(WeaponType.WEP_NONE))
                {

                    return (int)wep.GetWeaponDPS() * (1f + Attributes.TotalPrimaryAttributes.Dexterity / 100f);
                }
                else
                {
                    return (int)(1f * (1f + Attributes.TotalPrimaryAttributes.Dexterity / 100f));
                }
            }
        }        
    }
}
