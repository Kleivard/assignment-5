﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RoleplayingGame.CustomException;

namespace RoleplayingGame
{       
    /// <summary>
    /// Hero is an abstract class.
    /// <para>Managing Leveling, Attribues, Gear, Equipment, Experiance and hero information</para>
    /// </summary>
    public abstract class Hero { 

        public string Name { get; set; }
        public abstract ClassType ClassType { set; get; }
        public Leveling Level { get; } = new Leveling();
        /// <summary>
        /// Total DPS for hero.
        /// </summary>
        public abstract float DPS { get;}
        public AttributeHandler Attributes { get; set; } = new AttributeHandler();
        public GearManager GearManager { get; set; } = new GearManager();
        /// <summary>
        /// Configure compatability with empty slot and setup attributes.
        /// <para>Tip: Should always be called in subclass constructor</para>
        /// </summary>
        public void Init()
        {
            GearManager.ArmorCompatibility.Add(ArmorType.ARMOR_NONE);
            GearManager.WeaponCompatibility.Add(WeaponType.WEP_NONE);

            Attributes.InitAllAttributes();
            Console.WriteLine("A new hero just entered the console window! ");
        }
        /// <summary>
        /// Equip item, takes a weapon or armor.
        /// <para>Check Level Requirment, Weapon/Armor compatibility and slot availability. If any checks are incorrect
        /// it will throw and exception.</para>
        /// </summary>
        /// <param name="weapon"></param>
        public void EquipItem(Weapon weapon)
        {
            try
            {
                GearManager.CheckLevelRequirement(weapon, Level.CurrentLevel);
                GearManager.CheckWeponTypeCompatibility(weapon.WeaponType);
                GearManager.IsItemSlotAvailable(weapon.ItemSlot);
                
                    GearManager.EquipItem(weapon);
            }
            catch (LevelException ex)
            {
                Console.WriteLine(ex + $"Item's item level ({weapon.ItemLevelRequirment}) has higher then hero level ({Level.CurrentLevel}). Cannot equip item.");
            }
            catch (InvalidWeaponException ex)
            {
                Console.WriteLine(ex + $" Armor type ({weapon.WeaponType}) is not compatible with hero class ({ClassType}). Cannot equip item.");
            }
            catch (GearException ex)
            {
                Console.WriteLine(ex + $" Hero already got an item equiped in itemslot ({weapon.ItemSlot}).");
            }
        }

        public void EquipItem(Armor armor)
        {
            try
            {
                GearManager.CheckLevelRequirement(armor, Level.CurrentLevel);
                GearManager.CheckArmorCompatibility(armor.ArmorType);
                GearManager.IsItemSlotAvailable(armor.ItemSlot);
                
                         GearManager.EquipItem(armor);
                         Attributes.IncreaseEquippedPrimaryAttributes(armor.ArmorAttributes);
                         Attributes.UpdateAllAttributes();
                
            }
            catch (LevelException ex)
            {
                Console.WriteLine(ex + $" Item's item level ({armor.ItemLevelRequirment}) has higher then hero level ({Level.CurrentExperiance}). Cannot equip item.");
            }
            catch (InvalidArmorException ex)
            {
                Console.WriteLine(ex + $" Item type ({armor.ArmorType}) is not compatible with hero class ({ClassType}). Cannot equip item.");
            }catch (GearException ex) 
            {
                Console.WriteLine(ex + $" Hero already got an item equiped in itemslot ({armor.ItemSlot}).");
            }
        }
        /// <summary>
        /// Removes item from gear and replaces it with Deafult Empty slot item.
        /// <para>If armor is removed it will reevaluate hero attributes</para>
        /// <para>If slot is empty. Nothing will happen.</para>
        /// </summary>
        /// <param name="itemslot">Item slot to unequip</param>
        public void UnEquipItem(ItemSlot itemslot)
        {
            if (!GearManager.IsItemSlotAvailable(itemslot)) { 

                if (itemslot.Equals(ItemSlot.SLOT_WEAPON))
                {
                    GearManager.UnEquipWeapon(itemslot);
                }
                else
                {
                    Armor currentArmor = (Armor)GearManager.Gear[itemslot];
                    Attributes.DecreaseEquippedPrimaryAttributes(currentArmor.ArmorAttributes);
                    Attributes.UpdateAllAttributes();
                    GearManager.UnEquipArmor(itemslot);
                }
            }
            else
            {
                Console.WriteLine("Nothing to unequip !");
            }
        }
        /// <summary>
        /// Increased the heros current experiance.
        /// <para>At each level it will increase the base primary attributes and reevaluate all attributes</para>
        /// </summary>
        /// <param name="exp">Amount of experiance</param>
        public void GainedExperiance(float exp)
        {
            // If experiance is worth more than 2 levels it will loop until the correct level.
            do
            {
                if (Level.IncreaseExperiance(exp))
                {
                    Attributes.IncreaseBasePrimaryAttributes();
                    Attributes.UpdateAllAttributes();
                    Console.WriteLine($" {Name} reached level {Level.CurrentLevel} ! {Level.ExperianceForNextLevel - Level.CurrentExperiance} exp until level {Level.CurrentLevel + 1} ");
                }
            } while (Level.CurrentExperiance >= Level.ExperianceForNextLevel) ;

        }
        /// <summary>
        /// Prints hero summary to the console. 
        /// <para>Contains: Name, class, level, primary and secondary stats dps</para>
        /// </summary>
        public void CharacterStatsDisplay()
        {
            StringBuilder sb = new("\n----------------  Character Stats Window  ----------------------\n");
            sb.Append($"Name:\t{Name}\n");
            sb.Append($"Class:\t{ClassType}\n");
            sb.Append($"Character level: {Level.CurrentExperiance}  -->  remaining XP for next level is {Level.ExperianceForNextLevel - Level.CurrentExperiance}\n");
            sb.Append("\nPrimary stats:\n\n");
            sb.Append(Attributes.TotalPrimaryAttributes.ToString());
            sb.Append("\nSecondary stats:\n\n");
            sb.Append(Attributes.SecondaryAttributes.ToString());
            sb.Append($"\nDPS:\t{DPS}\n");
            sb.Append("----------------------------------------------------------------\n");
            Console.WriteLine(sb);
        }
        /// <summary>
        /// Print hero DPS to console
        /// </summary>
        public void DisplayDPS() 
        {
            Console.WriteLine("\n---------------------------  DPS ------------------------------");
            Console.WriteLine(DPS);
            Console.WriteLine($"{Name} current DPS is {DPS}");
            Console.WriteLine("--------------------------------------------------------------------\n");
        }
        /// <summary>
        /// Print hero Level to console
        /// </summary>
        public void DisplayLevel()
        {
            Console.WriteLine("\n---------------------------  XP .. ------------------------------");
            Console.WriteLine($"Level: {Level.CurrentLevel}, CurrentXP: {Level.CurrentExperiance}, RemainingXP:  {Level.ExperianceForNextLevel - Level.CurrentExperiance} ");
            Console.WriteLine("--------------------------------------------------------------------\n");
        }
        /// <summary>
        /// Print heros equipped gear to console
        /// </summary>
        public void DisplayGear()
        {
            Console.WriteLine("\n---------------------------  GEAR  ------------------------------");
            Console.WriteLine($"" +
                            $"Head: {GearManager.Gear[ItemSlot.SLOT_HEAD].ItemName},\n" +
                            $"Body: {GearManager.Gear[ItemSlot.SLOT_BODY].ItemName},\n" +
                            $"Legs: {GearManager.Gear[ItemSlot.SLOT_LEG].ItemName},\n" +
                            $"Weapon: {GearManager.Gear[ItemSlot.SLOT_WEAPON].ItemName}");
            Console.WriteLine("--------------------------------------------------------------------\n");
        }
        /// <summary>
        /// Print all the heros attribues to console
        /// </summary>
        public void DisplayStats()
        {
            Console.WriteLine("---------------------------  STATS  ------------------------------\n");
            Console.WriteLine("Stats total:\n" + Attributes.TotalPrimaryAttributes.ToString());
            Console.WriteLine("Stats equip:\n" + Attributes.EquippedPrimaryAttributes.ToString());
            Console.WriteLine($"Stats base:\n" + Attributes.BasePrimaryAttributes.ToString());
            Console.WriteLine("------------------------------------------------------------------\n");
        }
    }
}
