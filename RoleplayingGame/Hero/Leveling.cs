﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoleplayingGame
{
    /// <summary>
    /// This class is used to enable a leveling system for an hero.
    /// <para>Keeps track of current level, current experiance, and experiance for next level.</para>
    /// </summary>
    /// 
    public class Leveling
    {


        public int MyProperty { get; set; }

        public int CurrentLevel { get; set; } = 1;

        public float CurrentExperiance { get; set; } = 0;

        public float ExperianceForNextLevel { get; set; } = 100;

        /// <summary>
        /// Increases the current experiance based on arguemnt input. 
        /// <para>If experiance is enough to level up the level is increased by 1 and experiance for the next level is increased.</para>
        /// </summary>
        /// <param name="experiance"> float value that adds to current experiance </param>
        /// <returns>Returns true if leveled is increased. Else it returns false</returns>
        public bool IncreaseExperiance(float experiance) {
            if (experiance <= 0)
            {
                throw new ArgumentException("Cannot get zero or less experiance");
            }
            CurrentExperiance += experiance;
            if (CurrentExperiance >= ExperianceForNextLevel)
            {
                CurrentLevel += 1;
                ExperianceForNextLevel = ExperianceForNextLevel * (float)1.337;
                return true;
            }
            else return false;
        }    
    }
}
