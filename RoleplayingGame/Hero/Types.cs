﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoleplayingGame
{
    /// <summary>
    /// Enum with all the different weapon types.
    /// WEP_NONE should be used when there is no weapon equipped
    /// </summary>
    public enum WeaponType
    {
        WEP_AXE,
        WEP_BOW,
        WEP_DAGGER,
        WEP_HAMMER,
        WEP_STAFF,
        WEP_SWORD,
        WEP_WAND,

        WEP_NONE
    }
    /// <summary>
    /// Enum with all the different armor types.
    /// ARMOR_NONE should be used when there is no armor equipped.
    /// </summary>
    public enum ArmorType
    {
        ARMOR_CLOTH,
        ARMOR_LEATHER,
        ARMOR_MAIL,
        ARMOR_PLATE,

        ARMOR_NONE
    }

    /// <summary>
    /// These are the items slot availible for a hero to equip items in.
    /// </summary>
    public enum ItemSlot
    {
        SLOT_HEAD,
        SLOT_BODY,
        SLOT_LEG,
        SLOT_WEAPON
    }
    /// <summary>
    /// These are the class types a hero can be.
    /// </summary>
    public enum ClassType
    {
        WARRIOR,
        ROUGE,
        RANGER,
        MAGE
    }
}
