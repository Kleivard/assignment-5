﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoleplayingGame
{
    /// <summary>
    /// Do NOT look at this code. Implement the class if you want. BUT DO NOT LOOK AT THE CODE FOR PETE SAKE.
    /// </summary>
    public class PlaySomeGames
    {
        public bool IsPlaying { get; set; } = true;
        public Hero Hero { get; set; }

        public List<Armor> armorInventory = ArmorSeed.Instance;
        public List<Weapon> weaponInventory = WeaponSeed.Instance;

        public void CharacterCreationMenu()
        {

            while (IsPlaying)
            {
                Console.WriteLine("Hello, please pick an option:");
                Console.WriteLine("1. Create new hero");

                switch (GetUserInput())
                {
                    case 1:
                        CreateHero();
                        PlayAroundWithHero();
                        break;
                    case 2:
                        //ChangeHero();
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        break;
                    default:
                        Console.WriteLine("Now an option mate");
                        break;
                }
            }
        }


        public void CreateHero() {
            Console.WriteLine("Pick a class");
            Console.WriteLine("1. Mage");
            Console.WriteLine("2. Rouge");
            Console.WriteLine("3. Ranger");
            Console.WriteLine("4. Warrior");
            string name;

            switch (GetUserInput())
            {
                case 1:
                    Console.WriteLine("You picked Mage, give your hero a name !");
                    name = Console.ReadLine();
                    Hero = new Mage() { Name = name };
                    break;
                case 2:
                    Console.WriteLine("You picked Rouge, give your hero a name !");
                    name = Console.ReadLine();
                    Hero = new Rouge() { Name = name };
                    break;
                case 3:
                    Console.WriteLine("You picked Ranger, give your hero a name !");
                    name = Console.ReadLine();
                    Hero = new Ranger() { Name = name };
                    break;
                case 4:
                    Console.WriteLine("You picked Warrior, give your hero a name !");
                    name = Console.ReadLine();
                    Hero = new Warrior() { Name = name };
                    break;
            }

            Console.WriteLine($"You have created a new hero named {Hero.Name} with the class specification {Hero.ClassType} ! ");
        }

        public void PlayAroundWithHero() {
            printHeroOptions();

            while (IsPlaying)
            {
                

                switch (GetUserInput())
                {
                    case 1:
                        Hero.DisplayLevel();
                        break;
                    case 2:
                        Hero.DisplayStats();
                        break;
                    case 3:
                        Hero.DisplayDPS();
                        break;
                    case 4:
                        Hero.DisplayGear();
                        break;
                    case 5:
                        printItems(weaponInventory);
                        EquipmentMenuWep();
                        break;
                    case 6:
                        printItems(armorInventory);
                        EquipmentMenuArm();
                        break;
                    case 7:
                        Console.WriteLine("\nType the amount of xp you want to give your hero !");
                        float xp = GetUserInput();
                        Hero.GainedExperiance(xp);
                        break;
                    case 8:
                        Hero.CharacterStatsDisplay();
                        break;
                    case 9:
                        printHeroOptions();
                        break;
                    default:
                        Console.WriteLine("Now an option mate");
                        break;
                }
            }

        }

        public void printItems(List<Armor> al) 
        {
            int i = 0;
            Console.WriteLine("\n---------------------------  ARMORS ------------------------------");
            Console.WriteLine("Armors available:");
            foreach (Armor a in al)
            {
                Console.WriteLine("Iteam " + i +$" Name: {a.ItemName}, stats: " + a.ArmorAttributes.ToString()+ $" Ilvl: {a.ItemLevelRequirment}" );
                i++;
            }
            Console.WriteLine("--------------------------------------------------------------------\n");

        }
        public void printItems(List<Weapon> wl)
        {
            int i = 0;
            Console.WriteLine("\n---------------------------  WEAPONS ------------------------------");

            Console.WriteLine("Armors available:");
            foreach (Weapon a in wl)
            {

                Console.WriteLine("Iteam " + i + $" Name: {a.ItemName}, DPS: " + a.GetWeaponDPS() + $" Ilvl: {a.ItemLevelRequirment}");
                i++;
            }
            Console.WriteLine("--------------------------------------------------------------------\n");
        }

        public int GetUserInput() {

            int myNum;
            while (!int.TryParse(Console.ReadLine(), out myNum)) 
            {
                Console.WriteLine("Try again");
            }
            return myNum;
        }

        public void EquipmentMenuWep()
        {
            Console.WriteLine("\nWhat do you want to do in Weapon inventory?");
            Console.WriteLine("1. Equip Weapon");
            Console.WriteLine("2. Leave\n");
            switch (GetUserInput()) {
                case 1:
                    Console.WriteLine("What weapon do you want to equip ? \n");
                    int iteamNumber = GetUserInput();
                    
                    if (!Hero.GearManager.Gear[weaponInventory[iteamNumber].ItemSlot].Equals(weaponInventory[iteamNumber]))
                    {
                        Hero.EquipItem(weaponInventory[iteamNumber]);
                        Console.WriteLine($"You just equiped {weaponInventory[iteamNumber].ItemName}, go and check your damage now!\n");
                    }
                    else 
                    {
                        Console.WriteLine("bro.... not compatible at all");
                        EquipmentMenuWep();
                    }    
                    break;
                case 2:
                    printHeroOptions();
                    break;
            }
        }

        public void EquipmentMenuArm()
        {
            Console.WriteLine("\nWhat do you want to do in Armor inventory?");
            Console.WriteLine("1. Equip Armor");
            Console.WriteLine("2. Leave\n");
            switch (GetUserInput())
            {
                case 1:
                    Console.WriteLine("What armor do you want to equip ? \n");
                    int iteamNumber = GetUserInput();

                    if (!Hero.GearManager.Gear[armorInventory[iteamNumber].ItemSlot].Equals(armorInventory[iteamNumber]))
                    {
                        Hero.EquipItem(armorInventory[iteamNumber]);
                        Console.WriteLine($"You just equiped {armorInventory[iteamNumber].ItemName}, go and check your stats !\n");
                    }
                    else
                    {
                        Console.WriteLine("bro.... not compatible at all");
                        EquipmentMenuArm();
                    }
 
                    break;
                case 2:
                    printHeroOptions();
                    break;
            }
        }

        public void printHeroOptions() {
            Console.WriteLine("\nWhat do you want to do with your hero?");
            Console.WriteLine("1. Check level information");
            Console.WriteLine("2. Check stats");
            Console.WriteLine("3. Check DPS");
            Console.WriteLine("4. Check gear");
            Console.WriteLine("5. Check weapon inventory");
            Console.WriteLine("6. Check armor inventory");
            Console.WriteLine("7. Add some experiance");
            Console.WriteLine("8. Character summary (Everything)");
            Console.WriteLine("9. Show options again\n");
        }
    }
}
